#!/bin/sh

cd $( dirname $0 )/

# working
./update-pkg aircrack-ng git https://github.com/aircrack-ng/aircrack-ng.git
./update-pkg beef-xss git https://github.com/beefproject/beef.git
./update-pkg dnsrecon git https://github.com/darkoperator/dnsrecon.git
./update-pkg johnny git https://github.com/shinnok/johnny.git
./update-pkg libfreefare git https://github.com/nfc-tools/libfreefare.git
./update-pkg libnfc git https://github.com/nfc-tools/libnfc.git
./update-pkg mfcuk svn https://github.com/nfc-tools/mfcuk.git
./update-pkg mfoc git https://github.com/nfc-tools/mfoc.git
./update-pkg set git https://github.com/trustedsec/social-engineer-toolkit.git
./update-pkg sqlmap git https://github.com/sqlmapproject/sqlmap.git

# not looked at yet
#./update-pkg airoscript-ng svn http://svn.aircrack-ng.org/branch/airoscript-ng/
#./update-pkg bloodhound https://github.com/BloodHoundAD/BloodHound.git
#./update-pkg empire https://github.com/BC-SECURITY/Empire.git
#./update-pkg exploitdb git https://github.com/offensive-security/exploitdb.git
#./update-pkg exploitdb-bin-sploits git https://github.com/offensive-security/exploitdb-bin-sploits.git
#./update-pkg exploitdb-papers git https://github.com/offensive-security/exploitdb-papers.git
#./update-pkg payloadsallthethings git https://github.com/swisskyrepo/PayloadsAllTheThings.git
#./update-pkg powersploit git https://github.com/PowerShellMafia/PowerSploit.git
#./update-pkg seclists git https://github.com/danielmiessler/SecLists.git
#./update-pkg wpscan git https://github.com/wpscanteam/wpscan.git
