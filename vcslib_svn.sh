vcs_parse_options() {
    :
}

vcs_get_current_revision() {
    LC_ALL=C svn info "$1" | sed -n -e 's/Revision: //p'
}

vcs_update() {
    svn update
}   

vcs_download() {
    svn co $1 $2
}   

vcs_export() {
    svn export . $2
}   

vcs_version_part() {
    date=$(date +%s)
    echo "$1+ts$date"
}
